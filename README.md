**Description of files**<br>
notes.txt - notes about the papers <br>
paper_list.txt - all "papers of the day", format: Date; DOI; Language; Topics; Title <br>
suggestion_for_paper.txt - list of papers to read in the future <br>

**Abbreviations**<br>
Languages:<br>
English (eng) <br>
German (ger)<br>

Topics:<br>
research data management (RDM)<br>
open access (OA)<br>
electronic laboratory notebooks (ELN)<br>
basics of pharmaceutical/medicinal chemistry (PMC)<br>
